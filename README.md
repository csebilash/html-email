# html-email
Email send with html template using ejs template engine on NodeJs


## Load HTML file : 
```
var template = fs.readFileSync('htmlTemplate.html',{encoding:'utf-8'});
```
## Render html file using ejs:

```
ejs.render(template,{value: value});
```


## Send Email:
```
transporter.sendMail({
from: from_email,
to: to_email,
subject: mail_subject,
html:  message 
});
```
