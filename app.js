var ejs = require('ejs');
var fs = require('fs');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: email,
        pass: password
    }
});


var template = fs.readFileSync('htmlTemplate.html',{encoding:'utf-8'});
var message = ejs.render(template,{
        value: value
    });

transporter.sendMail({
        from: from_email,
        to: to_email,
        subject: mail_subject,
        html:  message 
});
